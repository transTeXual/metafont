# trees

This project is for drawing syntax trees in Metapost. It arose out of a desire to draw trees for my syntax classes that fit my own particular style of diagramming. In particular, I like to:

1. place internal labels to the side of the tree 
2. keep left branches all the same length
3. lengthen right branches when needed

It took me about a week of tinkering in Metapost for a few hours each day to come up with the current version of the code. The syntax is not as simple as [qtree](https://ctan.org/pkg/qtree), say, but it is more functional and it complies with Metafont's somewhat strict syntax rules for certain tokens (like semicolons and square brackets).

As an example, `tree.mp` will draw the tree[^1]

[^1]: This example is from the syntax paper I happened to read most recently: <br> Collins, Chris. 2015. A smuggling approach to the passive in English. _Syntax_ 8: 81&ndash;120.

![](example.svg)

if given the code

    input tree;

    defaultfont := "cmr10mf";

    beginfig(0);
 
    {{; @SpecIP(<<"the book";>>.DP); }{1.5;
     {{I;\\"[+PST]"; }{;
      {{ V;\\be; }{1.75;
       {{; #SpecVoiceP({{ Part;\\"-en"; }{;
                        {{V;\\see;#DP(}{DP);}}.VP;
                       }}.PartP) dashed evenly; }{3;
        {{ Voice;\\by; }{;
         {{ John; }{;
          {{v; @PartP(}{PartP);
          }}.v`;
         }}.vP;
        }}.Voice`;
       }}.VoiceP;
      }}.VP;
     }}.I`;
    }}.IP;

    drawarrow PartP to SpecVoiceP;
    drawarrow DP to (SpecIP shifted 4left);

    endfig;

The syntax rules for `tree.mp` are as follows:

1. `{{label;` moves to the left child of the current node, and if specified, writes `label` there 

2. `}{label;` moves to the right sibling of the current node, and if specified, writes `label` there 

3. `}{n;` for `n` a number extends the right branch by `n` times the natural branch length

4. `}}label;` moves to the parent of the current node, and if specified, writes `label` there 

5. `\\label;` types `label` on the second line of the current node

6. `<<(label)>>.XP;` draws a triangle for `XP` above `label`

7. `@my_ind(...);` indexes any part of the tree in place of `...`

8. `#my_ind(...);` does the same as `@` but also draws a box

9. `drawarrow my_ind1 to my_ind2;` draws a movement arrow from index `my_ind1` to index `my_ind2`

The program also lets you type `"label"` (with double quotes) in place of `label` (without double quotes) above. Use this if you want labels containing `"multiple words with spaces between them"`; special Metafont symbols like `"[]"`, `"+"`, or `"-"`; or protected keywords like `"and"`, `"or"`, `"if"`, and `"not"`.

For convenience, I have also included in this directory a font `cmr10mf.pfb`/`cmr10mf.tfm`, which is Knuth's Computer Modern Roman 10pt font with a few changes to make it more convenient for Metafont and tree drawing, namely:

1. the space character (character 32) types explicit whitespace
2. `` ` `` (character 96) types a prime symbol (for bar-level phrases) rather than a left quote
3. `|` (character 124) types a strut (i.e. a no-width character 8.5pt in height and 3.5pt in depth), intended for aligning text

# notes
