# Keyword arguments in Metafont

This program adds support for a style of keyword arguments for use in defining functions.

## How to use `kwargs.mf`

To use the functionality in this program, you first define a function (call it `fx`) which accepts a `text` argument in its tail (call it `defaults`). In the body of the function, you include a line `setkeys(defaults)((var1,val1),(var2,val2),(var3,val3))` where each `val` is a default value that you'd like to have the corresponding `var` set at.

    def fx(expr x, y) text defaults =
        setkeys(defaults)((var1,val1),(var2,val2),(var3,val3));
        ...
    enddef

When you execute your function, you can optionally supply it with a list of equations (all wrapped in `"..."`, and delimited by `;`) which specifies new values for your `var`s. For example, you might say

    fx(x,y) "var1 = 1; var2 = 0; var3 = infty";

When `fx` is executed, it will use these new values instead of the defaults. If a new value is not given for any variable, the default value from the definition of `fx` will be used.

It's important to note that the equations don't have to be simple `x = y` statements. Anything accepted by Metafont's equation solver is allowed, for example something like

    fx(x,y) "var1 + var2 + var3 = 3;  var2 - var3 = 1";
